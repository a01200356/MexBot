#!/usr/bin/python3
"""
Relaciona municipios mexicanos en Wikidata
con sus ID de área geoestadística del INEGI.
"""
import pywikibot
from requests import get, post
from json import loads

sitio = 'wikidata'

# estados
wd_a_inegi = {
    "wikidata": {
        "Q1489": "09",
        "Q79952": "01",
        "Q58731": "02",
        "Q46508": "03",
        "Q80908": "04",
        "Q60123": "07",
        "Q655": "08",
        "Q53079": "05",
        "Q61309": "06",
        "Q79918": "10",
        "Q46475": "11",
        "Q60158": "12",
        "Q80903": "13",
        "Q13160": "14",
        "Q82112": "15",
        "Q79861": "16",
        "Q66117": "17",
        "Q79920": "18",
        "Q15282": "19",
        "Q34110": "20",
        "Q79923": "21",
        "Q79754": "22",
        "Q80245": "23",
        "Q78980": "24",
        "Q80252": "25",
        "Q46422": "26",
        "Q80914": "27",
        "Q80007": "28",
        "Q82681": "29",
        "Q60130": "30",
        "Q60176": "31",
        "Q80269": "32"
    },
    "test": {
        "Q53076": "14"
    }
}

mexico = {
    "wikidata": "Q96",
    "test": "Q53079"
}

id_municipios = {
    "wikidata": "P3801",
    "test": "P26570"
}

contains = {
    "wikidata": "P150",
    "test": "P110"
}

url_municipios = 'http://www.beta.inegi.org.mx/app/api/indicadores/interna_v1_1//API.svc/' \
                 + 'CatalogoAreaGeografica/ID_ESTADO/null/3/json/96fbd1bf-21e6-28e3-6e64-2b15999d2c89'
url_sparql = 'https://query.wikidata.org/sparql'
query_municipios = 'select ?item where{?item wdt:P31 wd:Q1952852; wdt:P131 wd:ID_ESTADO.}'
query_delegaciones = 'select ?item where{?item wdt:P31 wd:Q2734310}'


# para poder comparar los nombres
def normaliza_nombre(nombre):
    nombre = nombre.replace('Municipio de ', '')
    nombre = nombre.replace(' Municipality', '')
    nombre = nombre.split(' (')[0].split(' ,')[0]
    return nombre


if __name__ == "__main__":
    if sitio == 'wikidata':
        choice = pywikibot.input_yn('Aguas... este es el de a de veras. ¿Estás seguro?')
        if not choice:
            exit()

    repo = pywikibot.Site(sitio, 'wikidata').data_repository()

    # itera todos los estados
    país = pywikibot.ItemPage(repo, mexico[sitio]).get()
    for estado in país['claims'][contains[sitio]]:
        estado = estado.getTarget()

        municipios = []
        municipios_ids = []

        # busca municipios en inegi
        res = get(url_municipios.replace('ID_ESTADO', wd_a_inegi[sitio][estado.id]))
        municipios_json = loads(res.text)
        for municipio in municipios_json[0]['Municipio']:
            id_inegi = municipio['Municipio']
            nombre = municipio['Nombre']
            if int(id_inegi) > 0:
                municipios_ids.append(id_inegi)
                municipios.append({'id_inegi': wd_a_inegi[sitio][estado.id] + id_inegi,
                                   'nombre': nombre})

        # busca municipios en wikidata
        if sitio == 'wikidata':
            # si es CDMX, busca delegaciones en lugar de municipios
            if estado.id == 'Q1489':
                res = get(url_sparql, params={'query': query_delegaciones, 'format': 'json'})
            else:
                res = get(url_sparql, params={'query': query_municipios.replace('ID_ESTADO', estado.id),
                                              'format': 'json'})
            json_wd = loads(res.text)
        else:
            json_wd = {'results': {'bindings': [{'item': {'value': 'https://test.wikidata.org/wiki/Q53077',
                       'type': 'uri'}}]}, 'head': {'vars': ['item']}}

        for municipio_wd in json_wd['results']['bindings']:
            encontrado = False
            id_wd = municipio_wd['item']['value'].split('/')[-1]
            item = pywikibot.ItemPage(repo, id_wd)
            item_data = item.get()

            # busca que coincidan los nombres
            nombre_es = normaliza_nombre(item_data['labels'].get('es', ''))
            nombre_en = normaliza_nombre(item_data['labels'].get('en', ''))
            for municipio in municipios:
                if (nombre_es == municipio['nombre']):
                    encontrado = True
                elif (nombre_en == municipio['nombre']):
                    encontrado = True
                else:
                    for idioma in item_data['labels']:
                        nombre = normaliza_nombre(item_data['labels'][idioma])
                        if municipio['nombre'] == nombre:
                            encontrado = True
                            break

                if encontrado:
                    id_en_wd = None
                    # verifica si el municipio ya tiene id del inegi
                    if id_municipios[sitio] in item.claims:
                        claims = item.claims[id_municipios[sitio]]
                        ids_erroneos = []
                        for claim in claims:
                            id_inegi_segun_wd = claim.getTarget()
                            if id_inegi_segun_wd == municipio['id_inegi']:
                                id_en_wd = id_inegi_segun_wd
                            else:
                                ids_erroneos.append(claim)

                        # elimina ids que no coincidan con los datos del inegi
                        if len(ids_erroneos) > 0:
                            item.removeClaims(ids_erroneos, bot=True)
                            print('Se eliminaron ids erroneos' +
                                  ' para ' + municipio['nombre'] + '.')

                    # agrega id de inegi a wikidata si aún no está agregado
                    if not id_en_wd:
                        claim = pywikibot.Claim(repo, id_municipios[sitio])
                        claim.setTarget(municipio['id_inegi'])
                        item.addClaim(claim, bot=True)
                        print('Se agregó el id ' + municipio['id_inegi'] +
                              ' para ' + municipio['nombre'] + '.')
                    break
            else:
                print('No se encontró el municipio ' +
                      (nombre_es or nombre_en or id_wd) +
                      ' en el estado ' + estado.id + '.')
