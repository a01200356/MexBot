#!/usr/bin/python3
"""
Actualiza los datos poblacionales
de cada municipio de México
"""
import pywikibot
from requests import get
from json import loads

sitio = 'test'

población = {'wikidata': 'P1082',
             'test': 'P63'}

afirmado_en = {'wikidata': 'P248',
               'test': 'P287'}

fecha = {'wikidata': 'P585',
         'test': 'P74'}

método = {'wikidata': 'P459',
          'test': 'P282'}

censo = {'wikidata': 'Q39825',
         'test': 'Q12137'}

inegi = {'wikidata': 'Q795074',
         'test': 'Q32418'}

url_sparql = 'https://query.wikidata.org/sparql'
query_municipios = 'select ?item ?value where{?item wdt:P3801 ?value}'

url_api_inegi = 'http://www3.inegi.org.mx/sistemas/api/indicadores/v1/Indicador/1002000001/ID_LUGAR/es/false/json/'


def llave_api_inegi():
    with open('llave_inegi.txt') as archivo:
        return archivo.readline()[:-1]


# Agrega o actualiza información poblacional que falte
def agregar_población(repo, municipio, pob, año, declaración=None, falta_método=True, falta_fuente=True):
    if declaración is None:
        # Agrega población
        declaración = pywikibot.Claim(repo, población[sitio])
        declaración.setTarget(pywikibot.WbQuantity(pob, site=repo))
        item.addClaim(declaración, bot=True)

        # Agrega el año del censo
        fecha_censo = pywikibot.Claim(repo, fecha[sitio])
        fecha_censo.setTarget(pywikibot.WbTime(año))
        declaración.addQualifier(fecha_censo)

        print('Se agregó la población de ' + municipio.id + ' de ' + str(año) + '.')

    # El censo es el primer año de cada década, en otros años no es un censo
    if falta_método and int(año) % 10 == 0:
        # Agrega el método (censo)
        método_censo = pywikibot.Claim(repo, método[sitio])
        método_censo.setTarget(pywikibot.ItemPage(repo, censo[sitio]))
        declaración.addQualifier(método_censo)
        print('Se agregó como censo el método de la población de ' + municipio.id + ' de ' + str(año) + '.')

    if falta_fuente:
        # Agrega la fuente (INEGI)
        fuente = pywikibot.Claim(repo, afirmado_en[sitio])
        fuente.setTarget(pywikibot.ItemPage(repo, inegi[sitio]))
        declaración.addSource(fuente)
        print('Se agregó la fuente para la población de ' + municipio.id + ' de ' + str(año) + '.')


if __name__ == "__main__":
    if sitio == 'wikidata':
        choice = pywikibot.input_yn('Aguas... este es el de a de veras. ¿Estás seguro?')
        if not choice:
            exit()

    repo = pywikibot.Site(sitio, 'wikidata').data_repository()

    # Obtiene lista de items que tienen id de INEGI
    if sitio == 'wikidata':
        res_wd = get(url_sparql, params={'query': query_municipios, 'format': 'json'})
        json_wd = loads(res_wd.text)
        municipios = json_wd['results']['bindings']
    else:
        municipios = [{'item': {'value': '/Q64478'}, 'value': {'value': '09008'}},
                      {'item': {'value': '/Q64479'}, 'value': {'value': '09007'}},
                      {'item': {'value': '/Q42542'}, 'value': {'value': '13004'}}]

    for municipio_wd in municipios:
        # Obtiene página de Wikidata
        id_wd = municipio_wd['item']['value'].split('/')[-1]
        item = pywikibot.ItemPage(repo, id_wd)
        item_data = item.get()

        # Obtiene datos poblacionales de la API del INEGI
        id_inegi = municipio_wd['value']['value']
        res_inegi = get(url_api_inegi.replace('ID_LUGAR', id_inegi) + llave_api_inegi())
        json_inegi = loads(res_inegi.text)
        poblaciones = list(map(lambda d: (d['TimePeriod'], d['CurrentValue']), json_inegi['Data']['Serie']))

        # Itera datos poblacionales si tiene
        if población[sitio] in item.claims:
            for declaración in item.claims[población[sitio]]:

                pob = declaración.getTarget().amount

                fechas = declaración.qualifiers.get(fecha[sitio], [])
                if len(fechas) > 0:
                    año = fechas[0].getTarget().year

                # Busca si ya hay declaración con población y año dados por el INEGI
                tupla = (str(año), str(pob))
                if tupla in poblaciones:
                    # Elimina tupla de lista de poblaciones faltantes
                    poblaciones.remove(tupla)

                    # Determina si falta agregar censo como método
                    falta_método = False
                    if año % 10 == 0:
                        falta_método = True
                        métodos = declaración.qualifiers.get(método[sitio], [])
                        for met in métodos:
                            met = met.getTarget().id
                            if met == censo[sitio]:
                                falta_método = False
                                break

                    # Determina si falta agregar fuente
                    falta_fuente = True
                    for fuente in declaración.getSources():
                        fuentes = fuente.get(afirmado_en[sitio], [])
                        for fue in fuentes:
                            fue = fue.getTarget().id
                            if fue == inegi[sitio]:
                                falta_fuente = False
                                break

                    agregar_población(repo, item, pob, año,
                                      declaración=declaración,
                                      falta_método=falta_método,
                                      falta_fuente=falta_fuente)

        # Agrega la población para cada año disponible que falte
        for año, pob in poblaciones:
            agregar_población(repo, item, pob, año)
